export const pressTimeLimit = 45 //  wartość graniczna dla długiego i krótkiego wciśnięcia [ms]

interface MorseCodeDictionary {
    [key:string]: string
}
export const morseCodeDictionary:MorseCodeDictionary = {
    ".-" : 'a',
    "-..." : 'b',
    "-.-.": 'c',
    "-..": 'd',
    '---': 'o',
    '...': 's'
}
