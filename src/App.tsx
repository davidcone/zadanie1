import React, {useEffect, useState} from 'react';
import './App.css';
import Button from "./Components/Button/Button";
import {decodeMorse, translateTimeToSigns} from "./Services/morseCodeTranslatorService";

function App() {
    const [holdTimes, setHoldTimes] = useState<number[]>([])
    const [decodedSigns, setDecodedSigns] = useState<string[]>([])
    const [decodedLetters, setDecodedLetters] = useState<string[]>([])

    const handleClearMorse = () => {
        setHoldTimes([])
        setDecodedSigns([])
    }
    const handleDecodeMorse = () => {
        let decodedLetter = decodeMorse(holdTimes)
        if(decodedLetter){
            let tmpLetter = decodedLetter;
            setDecodedLetters((prevState => [...prevState,tmpLetter]))
        }

        handleClearMorse()
    }

    const holdTimeHandler = (time: number) => {
        setHoldTimes((prevState => [...prevState,time]))
    }
    useEffect(() => {
        const decoded = translateTimeToSigns(holdTimes)
        setDecodedSigns(decoded)
    }, [holdTimes]);

  return (
    <div className="App">
      <header className="App-header">
        <span><b>Morse</b> code translator</span>
      </header>
        <Button text='Wciśnij' getHoldTime={holdTimeHandler} />
        <button onClick={handleClearMorse}>Wyczyść</button>
        <button onClick={handleDecodeMorse}>Dekoduj</button>

        <div className='decoding white-box'>
            {decodedSigns.length > 0 ? decodedSigns.join(' '): 'Rozpocznij nadawanie'}
        </div>
        <h1>Zdekodowane słowo</h1> <button onClick={() => setDecodedLetters([])}>Wyczyść słowo</button>
        <div className='decoded white-box'>
            {decodedLetters.length > 0 ? decodedLetters.join('') : '-'}
        </div>
    </div>
  );
}

export default App;
