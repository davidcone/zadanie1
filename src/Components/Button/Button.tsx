import React, {FC, useState} from "react";


interface Props {
    text: string,
    getHoldTime: (pressTime:number) => void
}
const Button:FC<Props> = ({text,getHoldTime}) => {

    const [holdCounter, setHoldCounter] = useState(0)
    const interval = React.useRef<ReturnType<typeof setInterval>|null>(null)

    const startHoldCounter = () => {
        interval.current = setInterval(() => {
            setHoldCounter((prevState) => prevState + 1)
        },1)
    }
    const stopHoldCounter = () => {
        if(interval.current){
            clearInterval(interval.current)
            interval.current = null
            getHoldTime(holdCounter);
            setHoldCounter(0)
        }
    }

    return (
        <button
            onMouseDown={startHoldCounter}
            onMouseUp={stopHoldCounter}
            onMouseLeave={stopHoldCounter}
            onKeyDown={(event) => {if(event.keyCode === 32) startHoldCounter()}}
            onKeyUp={(event) => {if(event.keyCode === 32) stopHoldCounter()}}
        >
            {text}
        </button>
    )
}
export default Button
