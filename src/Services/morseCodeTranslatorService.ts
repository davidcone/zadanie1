import {morseCodeDictionary} from "../Constants/constants";
import {isShortPress} from "./detectShortLongPressService";

export const decodeMorse = (pressTimeArr:number[]) => {
    let morseCode: string =  translateTimeToSigns(pressTimeArr).join('')
    console.log(morseCode)

    if(morseCode in morseCodeDictionary){
        return morseCodeDictionary[morseCode]
    }else return false

}
export const translateTimeToSigns = (pressTimeArr:number[]) => {
    return pressTimeArr.map(time => isShortPress(time) ? '.' : '-')
}

