
import {pressTimeLimit} from "../Constants/constants";
export const isShortPress = (pressTimeInMs: number) => {
    return pressTimeInMs <= pressTimeLimit
}
